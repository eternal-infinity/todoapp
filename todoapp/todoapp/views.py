from django.shortcuts import render
from django.shortcuts import render
from .models import TodoListItem
from django.http import HttpResponseRedirect 

def todoappView(request):
    return render(request, 'todolist.html')

def todoappView(request):
    all_todo_items = TodoListItem.objects.all()
    return render(request, 'todolist.html',
    {'all_items':all_todo_items}) 

def addTodoView(request):
    is_private = request.POST.get('is_private', False)
    x = request.POST.get('content', '')
    y = request.POST.get('reminder', 0)
    z=''
    if (y==0):
        z="No"
    else:
        z="Yes"
    new_item = TodoListItem(content = x, reminder=z)
    new_item.save()
    return HttpResponseRedirect('/todoapp/') 

def deleteTodoView(request, i):
    y = TodoListItem.objects.get(id= i)
    y.delete()
    return HttpResponseRedirect('/todoapp/') 